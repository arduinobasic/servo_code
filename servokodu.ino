// Adafruit Motor shield library
// copyright Adafruit Industries LLC, 2009
// this code is public domain, enjoy!

#include <AFMotor.h>
#include <Servo.h> 

// DC hobby servo
Servo servo1;


void setup() {
  Serial.begin(9600);           // set up Serial library at 9600 bps
  Serial.println("Motor party!");

  // turn on servo
  servo1.attach(9);//plug to 9. pin to pwm leg of servo motor

  servo1.write(0);
  delay(3000);

  servo1.write(90);
  delay(3000);

  servo1.write(0);
}

// Test the DC motor, stepper and servo ALL AT ONCE!
void loop() {

}


